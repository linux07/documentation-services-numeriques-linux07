# Sommaire

### Documentation services Linux07

* [À propos](introduction/README.md)
  * [Infrastructure](introduction/infrastructure.md)
  * [Déploiement](introduction/deploiement.md)
  * [Sauvegardes](introduction/sauvegardes.md)

### Cloud Linux07

  * [Présentation](cloud-linux07/README.md)

  1. [1 - Premiers pas](cloud-linux07/premiers-pas/premiers-pas.md)
      * [1.1 - Découvrir la page d'accueil](cloud-linux07/premiers-pas/page-accueil.md)
      * [1.2 - Personnaliser les paramètres](cloud-linux07/premiers-pas/personnaliser-parametres.md)
      * [1.3 - Personnaliser le Tableau de bord](cloud-linux07/premiers-pas/tableau-bord.md)
  
  2. [2 - Gérer ses documents en ligne](cloud-linux07/gerer-documents-en-ligne/gerer-ses-documents-en-ligne.md) 
    * [2.1 - Créer un nouveau dossier](cloud-linux07/gerer-documents-en-ligne/creer-un-nouveau-dossier.md) 
    * [2.2 - Déposer un document](cloud-linux07/gerer-documents-en-ligne/deposer-document.md)
    * [2.3 - Déplacer des documents](cloud-linux07/gerer-documents-en-ligne/deplacer-documents.md)
    * [2.4 - Supprimer et restaurer des fichiers](cloud-linux07/gerer-documents-en-ligne/supprimer-restaurer-fichiers.md)
    * [2.5 - Restaurer une ancienne version d’un document](cloud-linux07/gerer-documents-en-ligne/restaurer-ancienne-version-document.md)
  
  3. [3 - Partager un fichier](cloud-linux07/partager-fichier/README.md)
    * [3.1 - Partager en interne](cloud-linux07/partager-fichier/partager-en-interne.md)
    * [3.2 - Partager à l'éxtérieur](cloud-linux07/partager-fichier/partager-exterieur.md)
  
  4. [4 - Discuter](cloud-linux07/discuter/README.md)
    * [4.1 - Céer une discussion](cloud-linux07/discuter/creer-discussion.md)
    * [4.2 - Commencer les échanges](cloud-linux07/discuter/commencer-echanges.md)
    * [4.3 - Discuter autour d'un document](cloud-linux07/discuter/discuter-autour-document.md)

  5. [5 - Collectifs](cloud-linux07/collectifs/README.md)  
    * [5.1 - Créer un collectif](cloud-linux07/collectifs/creer-collectif.md)
    * [5.2 - Ajouter des membres](cloud-linux07/collectifs/ajouter-membres.md)
    * [5.3 - Créer des pages](cloud-linux07/collectifs/creer-pages.md)

  6. [6 - Contacts](cloud-linux07/contacts/README.md)  
    * [6.1 - Créer un contact](cloud-linux07/contacts/creer-contact.md)
    * [6.2 - Créer un cercle](cloud-linux07/contacts/creer-cercle.md)
    * [6.3 - Partager un carnet d'adresses](cloud-linux07/contacts/partager-carnet-adresses.md)

  7. [7 - Agendas](cloud-linux07/agenda/README.md)
    * [7.1 - Gérer ses agendas](cloud-linux07/agenda/gerer-agendas.md)
    * [7.2 - Organiser ses événements](cloud-linux07/agenda/organiser-evenements.md)
    * [7.3 - Partager un Agenda](cloud-linux07/agenda/partager-agenda.md)

  8. [8 - Nextcloud sur mobile](cloud-linux07/nextcloud-mobile/README.md)  
    * [8.1 - Gérer ses fichiers sur mobile](cloud-linux07/nextcloud-mobile/gerer-fichiers-mobile.md)
    * [8.2 - Synchroniser ses Discussions](cloud-linux07/nextcloud-mobile/synchroniser-discussions.md)
    * [8.3 - Synchroniser ses contacts et son agenda](cloud-linux07/nextcloud-mobile/synchroniser-contacts-agenda.md)
    * [8.4 - Synchroniser ces Notes](cloud-linux07/nextcloud-mobile/synchroniser-notes.md)

  9. [9 - Nextcloud et Thunderbird](cloud-linux07/nextcloud-thunderbird/README.md) 
    * [9.1 - Synchroniser ses Contacts sur Thunderbird](cloud-linux07/nextcloud-thunderbird/synchroniser-contacts.md)
    * [9.2 - Synchroniser ses Agendas sur Thunderbird ](cloud-linux07/nextcloud-thunderbird/synchroniser-agendas.md) 


### Etherpad_Mypads Linux07
   * [Présentation](Etherpad_Mypads-Linux07/README.md)
   * [1 - Connexion MyPads](Etherpad_Mypads-Linux07/connexion/connexion-mypads.md)
     * [1.1 - Dossier MyPads](Etherpad_Mypads-Linux07/dossier/dossier-mypads.md)
     * [1.2 - Onglets MyPads](Etherpad_Mypads-Linux07/onglets/onglets.md)
     * [1.3 - Édition texte collaboratif](Etherpad_Mypads-Linux07/pad/pad.md)
     * [1.4 - Avancées et permissions des Dossiers et des Textes collaboratifs](Etherpad_Mypads-Linux07/permissions/permissions.md)

### Mail Linux07
### Linux07 Teams
### Mobilizon Linux07 
### Cryptpad Linux07
### Linux07date
### Libreto Linux07    