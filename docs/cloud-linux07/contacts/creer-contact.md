# Créer un contact
================

## Importer un fichier de contacts
-------------------------------

Plutôt que de recréer des contacts uns à uns, vous pouvez importer vos
répertoires issus d'autres logiciels et applications.

>[!NOTE]
> L'import n'est possible qu'avec des fichiers Virtual Contact File
> (**VCF/Vcard**) en **version 3.0 ou 4.0**.

1.  Cliquez sur Paramètres en bas à gauche de l'interface.

    ![](https://ahp.li/88a8d89985b2e9896cd3.png)

2.  (Optionnel) Si vous ne souhaitez pas mélanger plusieurs listes de contacts, créez un nouveau carnet d'adresse.

    ![](https://ahp.li/2e09434f4d0f40dd27e5.png)

    -   Entrez le nom de votre choix dans le champ de saisie et cliquez sur la flèche à droite pour valider.

3.  Cliquez sur "Importer les contacts".

    ![](https://ahp.li/526d832e7122a1d6335b.png)

4.  Une boîte de dialogue apparaît. Cliquez sur "Importer dans le carnet d'adresses Contacts" pour ouvrir la liste des différents carnets d'adresses (menu déroulant).

    ![](https://ahp.li/f0c4cc4a659109a290ae.png)

5.  Sélectionnez le carnet d'adresse dans lequel importer les nouveaux contacts.

    ![](https://ahp.li/528354ae0f54e596aa59.png)

6.  Cliquez sur "Sélectionnez un fichier local" pour importer un fichier(format .vcf) depuis votre ordinateur, ou sur "Importer depuis Fichiers" pour choisir un fichier (format .vcf) présent dans votre espace Nextcloud.

7.  Patientez pendant l'import de vos contacts. Ceux-ci apparaissent dans la section "Non groupé" de la liste des contacts, à gauche de l'interface.

8.  Vous pouvez choisir de désactiver l'affichage d'un carnet d'adresse dans les paramètres.
    -   Cliquez sur le menu d'action correspondant.
    -   Décochez la case "Activé" dans le menu déroulant.

    ![](https://ahp.li/085131bdf75106c356b5.png)

    -   Rechargez la page pour afficher les changements.

## Créer une entrée à la main
--------------------------

Si vous n'avez pas de fichier de contacts existant, il vous faudra créer
une fiche par contact à la main.

1.  Cliquez sur "Nouveau contact" en haut de la liste des contacts.

    ![](https://ahp.li/10131741e803ce980856.png)

2.  La fiche du contact apparaît dans la vue principale.

    ![](https://ahp.li/372ac557237b5974a77b.png)

    1.  Cliquez sur "Nouveau contact" dans la fiche pour indiquer le nom de votre contact. Vous pouvez aussi indiquer la société à laquelle il appartient, son titre au sein de la société et modifier son avatar.
        -   L'enregistrement est automatique.
    2.  Renseignez un numéro de téléphone, un email et/ou une adresse.
    3.  Cliquez dans le champ "Carnet d'adresses" pour faire apparaître la liste des carnets d'adresses et
        indiquer où ranger ce contact.
        -   Un contact ne peut être que dans un carnet à la fois. Cette limite se contourne en créant deux contacts identiques.
    4.  Cliquez dans le champ "Groupes" pour faire apparaître la liste des groupes déjà créés ou
        tapez directement un nouveau nom de groupe pour le créer (*amis, famille, clients, fournisseurs*...)
        -   Un contact peut être dans plusieurs groupes à la fois.
        -   Les groupes aparaissent en tant que section dans la liste des contacts.
    5.  Cliquez sur "Ajouter une nouvelle propriété" pour ajouter de nouveaux champs à la fiche (adresse
        supplémentaire, prononciation du nom, langues parlées, notes, fuseau horaire...)
        -   Une corbeille apparaît au survol de chaque propriété, permettant de la supprimer au besoin.
