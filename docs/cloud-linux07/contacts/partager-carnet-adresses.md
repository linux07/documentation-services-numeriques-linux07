# Partager un carnet d'adresses
==============================

Vous pouvez **partager** un carnet d'adresses à **d'autres
utilisateur⋅ices** (inscrit⋅es sur votre instance Nextcloud).

1.  Cliquez sur Paramètres en bas à gauche de l'interface.

    ![](https://ahp.li/88a8d89985b2e9896cd3.png)

2.  Cliquez sur le bouton de partage correspondant au carnet d'adresses que vous voulez partager.

    ![](https://ahp.li/71aba9891770ee6e0478.png)

3.  Entrez le nom des destinataires dans le champ de saisie qui apparaît et sélectionnez-les dans la liste déroulante.

4.  Accordez ou non l'autorisation de modifier le carnet d'adresses, ou révoquez le partage.

    Case "peut modifier" à cocher et icône corbeille à côté de chaque nom
    ![](https://ahp.li/907a3085002905020006.png)

> [!NOTE] 
> Il n'est pas possible de partager directement un carnet d'adresses
> avec l'extérieur, mais vous pouvez le télécharger (format .vcf) et
> ensuite le transmettre par le moyen de votre choix.
> 1. Cliquez sur le menu d'action correspondant au carnet d'adresses.
> 2. Choisissez "Télécharger" dans le menu déroulant.
> 
> ![](https://ahp.li/9069519672511ca1d310.png)