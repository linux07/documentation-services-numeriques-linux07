# Créer un Cercle
===============

Un cercle sert à **grouper des utilisateur⋅ices** de votre instance Nextcloud, dans l'idée de **partager** un agenda, des contacts, une
discussion, des notes (grâce au module Collectif), une liste de tâches
(grâce au module Deck) et des fichiers. Autrement dit, à **organiser du
travail collectif**.

1.  Cliquez sur le bouton **+** de la section Cercles dans la liste des contacts.

    ![](https://ahp.li/99b2fafd13da3cfd50fe.png)

2.  Une boîte de dialogue apparaît. Donnez un nom au cercle et cliquez sur "Créer un cercle".

    ![](https://ahp.li/3788071ecc5105881f95.png)

3.  Cliquez sur "Ajouter des membres" en haut du menu, à gauche de la vue principale.

    ![](https://ahp.li/e891157b3371438111aa.png)

4.  Une boîte de dialogue apparaît. Commencez à taper le nom de membres à ajouter dans le champ de saisie et cliquez dessus pour
    les sélectionner.

    ![](https://ahp.li/e78990600b4b07066066.png)

## Gérer les rôles
---------------

Quand vous créez un Cercle, vous en êtes automatiquement **propriétaire (*Owner*)**.

Vous pouvez accorder différents droits aux autres membres de la
discussion en cliquant sur le menu d'actions à droite de leur nom, dans la liste des membres.

![](https://ahp.li/7f44cd1bddf02322f27b.png)

-   Modérateur⋅ice (*Moderator)* : donne l'autorisation d'ajouter et de supprimer des **membres** du cercle.

-   Administrateur⋅ice (*Admin*) : donne l'autorisation de **modifier les paramètres** et d'ajouter et supprimer des **membres** et des **modérateu⋅ices** du cercle.

-   *Promouvoir en tant que seul propriétaire* : **remplace** **le⋅a propriétaire actuel⋅le**, donne l'autorisation de **supprimer le cercle**, de **modifier les paramètres** et d'ajouter et supprimer des **membres**, des **modérateur⋅ices** et des **administrateur⋅ices** du cercle.

    > [!NOTE] 
    > Dans le cas où le compte propriétaire est supprimé de
    > Nextcloud, l'administrateur⋅ice le⋅a plus ancien⋅ne du cercle
    > devient propriétaire.

## Définir les paramètres
----------------------

Un Cercle peut être public ou privé selon le degré de visibilité que vous lui accordez. Les réglages se font dans la vue principale.

![](https://ahp.li/daa3636f3f1d2f1f6132.png)

-   *Tout le monde peut demander l'adhésion* : le cercle est **ouvert** à tous⋅tes les utilisateur⋅ices.

-   *Les membres doivent accepter les invitations* : ajouter un⋅e membre déclenche l'envoi d'une **invitation qui doit être acceptée** par le⋅a destinataire.

-   *Les adhésions doivent être confirmées/acceptées par un modérateur (nécessite Open)* : les utilisateur⋅ices doivent **faire une demande** pour devenir membre.

    Bouton "Demander à rejoindre" dans la vue principale d'un cercle public
    ![Bouton "Demander à rejoindre" dans la vue principale d'un cercle public](https://ahp.li/f1c530035d3a138963a3.png)**

    Le bouton notification est le deuxième dans le groupe à droite de la barre d'accès
    ![Le bouton notification est le deuxième dans le groupe à droite de la barre d'accès](https://ahp.li/347fa155996dced1e6e6.png)

    > [!WARNING] 
    > Cette condition nécessite d'avoir aussi coché "Tout le monde
    > peut demander l'adhésion".

-   *Les membres peuvent aussi inviter* : **les membres ont la
    possibilité d'inviter** d'autres utilisateur⋅ices dans le Cercle (sans passer par un⋅e modérateur⋅ice).

-   *Visible à tous* : le cercle **apparaît dans la liste de contacts** de tous⋅tes les utilisateur⋅ices, ainsi que dans la **fonction recherche**. Si la case est décochée, seul⋅es les membres du cercle le verront apparaître dans leur liste.

    ![la fonction recherche est le premier bouton dans le groupe à droite de la barre d'accès](https://ahp.li/9a84403835ad8a27019f.png)
