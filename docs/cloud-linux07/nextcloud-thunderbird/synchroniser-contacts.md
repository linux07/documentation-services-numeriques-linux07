# Synchroniser ses contacts avec Thunderbird
=======================================================

> [!WARNING]
> Depuis peu Thunderbird a eu une mise à jour importante vers une nouvelle version numérotée 102, celle-ci vient améliorer bien des éléments de l'application open source. Si votre version est antérieure, je vous conseille vivement dans un premier temps de mettre à jour Thunderbird avant de continuer.

===

> [!NOTE]
> Avant cette nouvelle version, pour bien synchroniser les Contacts et Agendas de Nextcloud, étaient nécessaires 3 modules complémentaires (extensions) de Thunderbird, Cardav&Caldav Provider, TBSync et Category Manager. Ce n'est plus necessaire. Il ne reste qu'une extension que je conseille encore de rajouter, Cardbook.


La nouvelle interface de Thunderbird ajoute une barre à gauche de sa fenêtre où se trouve les icônes pour les Courriers, les Contacts, les Agendas, les Tâches, les Discussions et autre modules si ajoutés. 

## 1. Ouvrez l'onglet **Contacts** en cliquant sur son icône 

![Onglet Contacts](https://ahp.li/a4ff47aae0aea527a07a.png)

L'onglet des Carnets d'adresses s'ouvre

![Interface du Carnet d'adresses](https://ahp.li/cf107fcb65bb2fe1ff67.png)

## 2. Ajouter un carnet d'adresse

Cliquez sur Nouveau carnet d'adresses, puis sur Ajouter un carnet d'adresses CardDAV

![Nouveau carnet adresses Cardav](https://ahp.li/99d46a9f60f7df607725.png)

Une fenêtre popup s'affiche où vous devez entrer votre nom d'utilisateur Nextcloud et l'adresse web de votre serveur Nextcloud.

![Identifiant et adresse serveur](https://ahp.li/f5982bbbed02db2ef3f1.png)

Cliquez ensuite sur Continuer. Une nouvelle fenêtre popup s'ouvre pour vous authentifier auprès du serveur Nextcloud. Entrez votre nom d'utilisateur et votre mot de passe Nextcloud.

![Authentification Nextcloud](https://ahp.li/d90bf11d77df5c33c82d.png)

Vérifiez aussi que la case _Utilisez le gestionnaire de mot de passe pour se souvenir de ce mot de passe_ en bien cochée, puis cliquez sur OK

Il faut patienter un peu pour qu'il trouve le carnet ou les carnets d'adresses, puis la liste va apparaître.

![Liste carnets d'adresses](https://ahp.li/9a89495b8429e9a96c55.png)

On peut choisir le carnet ou les carnets d'adresses que l'on veut synchroniser.

![Choix des carnets d'adresses à synchroniser](https://ahp.li/64e6764a6a08820b3549.png)

Puis on clique sur Continuer, on attends que la synchronisation soit finie. Les carnets se retrouvent dans l'onglet de gauche avec la liste des utilisateurs et de leur adresses courriel.

![Carnets adresses synchronisés](https://ahp.li/72b071b72ac71640f764.png)

## 3. Utiliser les Groupes comme des listes de courriels

Une fonction très utile, les Groupes dans les Contacts de Nextcloud peuvent servir à créer des listes de mails, un utilisateur peut appartenir à plusieurs Groupes. Pour cela, nous avons besoin du module CardBook de Thunderbird.


Sur la page Courrier (l'onglet Courrier entrant), en haut à droite se trouve une icône sandwich (trois petite ligne), en cliquant dessus on déplie le **menu** et on va descendre sur **Module complémentaires et thème**


![menu module complémentaires et thèmes](https://ahp.li/3e49b49b5b5bbbb8d586.png)

Ensuite on va cliquer sur **Extensions** dans la colonne de gauche et dans le champ de recherche on tape _cardbook_ et on clique sur la loupe.

![Recherche Cardbook](https://ahp.li/2103236a573689772819.png)

Une page des modules de Thunderbird va s'ouvrir et vous pourrez cliquer sur Installer Cardbook

![Installation Cardbook](https://ahp.li/147481faef4e88802808.png)

Après l'installation, il  a une petite configuration post-installation, cliquez sur _Suivant_

![Configuration Cardbook](https://ahp.li/14b7d547b1061d02ff46.png)

Il va vous proposez les carnets d'adresses qu'il a trouver.

![Choix carnets cardbook](https://ahp.li/64e6764a6a08820b3549.png)

Vous pouvez changer des couleurs, les noms des carnets, je vous conseille de garder par défaut _Travailler hors connexion_ coché et _vCard 3.0_.

![Choix carnets Cardbook](https://ahp.li/de55c499cab07d95e89c.png)

La dernière fenêtre devrait  afficher un message: _Vos carnets d'adresses ont été configurés avec succès_
Cliquer sur Suivant.

Vous avez maintenant une nouvelle icône pour ouvrir CradBook dans la colonne de droite tout en bas. 

Au-dessus des carnets d'adresses un bouton pour synchroniser les Carnets d'adresses liés avec le serveur Nextcloud.

Si vous dépliez un carnet d'adresse, vous verrez les fameux Groupes dans Nextcloud, CardBook appelle ces Groupes des Catégories.


![Vue Cardbook](https://ahp.li/42ab305481b39d13affc.png)

Par exemple, si je vais sur une categorie, je vois la liste des contacts qui sont dans cette categorie. 

![Vue courriels categorie](https://ahp.li/09f4e7a20addf450cb9a.png)

Cette categorie peut me servir de liste de courriels. Si je me lance dans _Écrire un nouveau message_, s'ouvre la fenêtre de rédaction. Pour afficher la _Barre de contacts_, soit je clique sur la touche F9, soit dans l'onglet _Affichage_ je clique sur _Barre de contacts_

![Affichage barre de contacts](https://ahp.li/18a96b555907a797b9f6.png)

Dans la barre de Contacts, on peux choisir:
* le Carnet d'Adresses (ou Tous les carnets d'adresses), 
* Filtrer sur les Catégories, 
* chercher un contact. 

Vous aurez tous les contacts d'un Carnet et d'une Catégorie si elle est choisie, avec une icône d'une personne et les Categories (liste courriels) avec une icône de groupe (deux personnes).

Si vous choisissez une Catégorie ou un contact, vous pouvez en-dessous choisir:
* si le message lui est adressé, 
* en Copie (CC), 
* Copie cachée (CCi) 
* ou Répondre à.


![Barre de Contacts choix](https://ahp.li/b2e836bdf4263bbb932d.png) 

> [!TIP]
> Si vous avez beaucoup d'adresses dans une Catégorie, préférez Ajout Copie cachée. Les longues listes de courriel ne sont pas très sûres en matière de sécurité.

## Éditer un contact

Privilégiez CardBook pour gérer vos contacts. Voyons comment éditer un contact

Cliquer sur le contact. vous pouvez utiliser le bouton _Modifier_ dans la barre en haut à droite, ou juste double-cliquer sur le contact pour l'éditer.

![Cardbook choix utilisateur](https://ahp.li/960b07742700f63470ff.png)

Vous pouvez choisir de le mettre dans un autre carnet d'adresses en dépliant les carnets d'adresses.

![modifier carnets d'adresses](https://ahp.li/81c364ef4a28adfc42fd.png)

Vous pouvez choisir dans quelles categories ajouter le contact (comme expliqué au-dessus, très pratique pour avoir des listes mails)

![modifier catégories](https://ahp.li/3967d9cd679db5a8a377.png)

En descendant vous avez aussi plusieurs informations à ajouter ou modifier. avec le **+** devant l'adresse courriel, vous pouvez attribuer plusieurs adresses courriels, ajouter un ou des numéros de téléphones, ajouter une ou des adresses, des comptes de communication VOIP sur internet (Google Talk, Skpe, Jammi, Jabber), des URLS de site, et évenements ??

![modifier options](https://ahp.li/e38a2993f683b3e9eb28.png)

> [!Note]
> N'oubliez pas de cliquer sur _Sauver_ en bas de la fenêtre pour sauvegarder vos modifications. Si vous créez un nouveau contact, vous aurez les mêmes options qu'ici, évidemment