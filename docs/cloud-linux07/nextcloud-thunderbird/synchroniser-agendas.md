# Synchroniser ses agendas avec Thunderbird
======================================================

> [!WARNING]
> Depuis peu Thunderbird a eu une mise à jour importante vers une nouvelle version numérotée 102, celle-ci vient améliorer bien des éléments de l'application open source. Si votre version est antérieure, je vous conseille vivement dans un premier temps de mettre à jour Thunderbird avant de continuer.

===

> [!NOTE]
> Avant cette nouvelle version, pour bien synchroniser les Contacts et Agendas de Nextcloud, étaient nécessaires 3 modules complémentaires (extensions) de Thunderbird, Cardav&Caldav Provider, TBSync et Category Manager. Ce n'est plus necessaire. Il ne reste qu'une extension que je conseille encore de rajouter, Cardbook.


La nouvelle interface de Thunderbird ajoute une barre à gauche de sa fenêtre où se trouve les icônes pour les Courriers, les Contacts, les Agendas, les Tâches, les Discussions et autre modules si ajoutés.

## 1. Ouvrez l'onglet **Agendas** en cliquant sur son icône 

![Onglet Agendas](https://ahp.li/33cee7d4eee444e0eb1e.png)

L'onglet des Agendas s'ouvre

## 2. Cliquez sur créer un nouvel Agenda

En cliquant sur le **+** devant Agendas 

![créer un nouvel agenda](https://ahp.li/0c4efdc235ff2ff48d94.png)


une fenêtre popup va s'ouvrir, choisissez Sur le réseau

![créer un nouvel agenda sur le réseau](https://ahp.li/4cac5454c4ea293a4442.png)


Puis _Suivant_, Une nouvelle fenêtre s'ouvre, vous devez mettre votre nom d'utilisateur et l'adresse internet de votre instance Nextcloud (ici celle du Cloud Linux07)

![nouvel agenda identifiant et url](https://ahp.li/2019971b484b1b0010f1.png)

Puis cliquez sur _Chercher de Agendas_, une fenêtre popup va vous demander de vous authentifier avec votre mot de passe de l'instance Nextcloud

![nouvel agenda authentifiaction nextcloud](https://ahp.li/206c4b9611bd91e9512f.png)

Mettez votre mot de passe et pensez à cocher _Utiliser le gestionnaire de mot de passe pour se souvenir de ce mot de passe_

Attendre un peu qu'il recherche les agendas, vous allez voir la liste des Agendas et ceux partagés avec vous que vous avez sur votre instance Nextcloud.

![Séléctions Agendas](https://ahp.li/58b6daaabc0a403d0e53.png)

Vous avez le choix de cocher ceux que vous voulez synchroniser avec Thunderbird en les gardant cochés. Il y aussi un onglet Propriétés, qui est aussi accessible plus tard.

Cliquez ensuite sur _S'abonner_.


Les Agendas sélectionner vont apparaître. Voilà c'est fait !

## L'interface et les options

Une fois les agendas importés.

![agendas thunderbird](https://ahp.li/a40d5c190322293d1c02.png)

1. Dans la barre de navigation en haut on a _synchroniser_ (synchroniser les agendas entre Thunderbird et l'instance Nextcloud),_Évenements_ (créer un nouvel événement), _Tâche_ (ajouter une tâche, si l'agenda est un agenda avec des tâches), _Modifier_ (modifier un événement sur le calendrier, cliquer dessus auparavant), _Supprimer_ (supprimer un événement, cliquer dessus auparavant).
2. Un espace ou sont listés les évenements.
3. Un outil de navigation pour avancer ou reculer dans le temps.

![choix liste événement et options](https://ahp.li/d939ed5298d38e157058.png)


1. En haut à droite, vous avez un bouton pour les synchroniser,après un changement effectué. 
2. Un choix pour la liste des événements à voir au-dessus du calendrier (tous, du jour, semaine, mois, futurs, etc...) 
3. devant une barre de recherche par mot clé (avec une loupe).
4. au-dessus de l'agenda, des onglets pour une vue par journée, semaine, multisemaine ou mois 

En se positionnant sur un agenda et avec clic droit on a des options qui apparraîssent

![agenda choix clic droit propriétés](https://ahp.li/5cacdcdb77f607adf0af.png)

Choix d'affichages (vous pouvez aussi simplement cliquer sur l'icône avec un oeil devant un agenda pour le masquer ou le voir), nouvel Agenda (créer un nouvel Agenda), se désabonner de l'agenda, exporter l'agenda, publier l'agenda, puis synchroniser les agendas et Propriétés. 

Cliquont sur Propriétés de l'agenda, une fenêtre s'ouvre pour modifier un agenda

![modifier agenda](https://ahp.li/8be28aa21148a581a03e.png)

Vous pouvez changer le nom de l'agenda, changer sa couleur, choisir le temps imparti avant l'actualisation de l'agenda (sa synchronisation), vous pouvez le lier à une autre adresse mail si vous en avez plusieurs configurées, puis des réglages pour les notifications.
Cliquer sur OK, une fois fini.

Pour créer un événement, vous pouvez aussi simplement faire un double clic dans une date choisie du calendrier, une fenêtre va s'ouvrir pour sa création.

![créer événement](https://ahp.li/afc3cb0443768ca689ab.png)

Vous pouvez choisir
* à quel Agenda attribuer cet événement, 
* le Titre,
* le Lieu,
* la Catégorie (vous pouvez créer aussi de catégories d'événements),
* si c'est sur la journée entière cochez Évenement sur la journée, sinon date et heure du début et date et heure de la fin,
* Répétition (permet de répéter un événement sur un choix de laps de temps)
* Description de l'événement

> [!NOTE]
> Quand vous êtes satisfait, n'oubliez pas de cliquer sur _Enregistrer et fermer_ dans la barre du haut, sinon vous aurez perdu votre édition...

Je m'arrêtte là, car l'essentiel y est pour moi, même si il reste les fonctions comme inviter des perticipants, confidentialité ou joindre, qui peuvent avoir un intérêt... 


