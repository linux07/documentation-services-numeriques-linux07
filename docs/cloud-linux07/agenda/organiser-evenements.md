# Organiser ses événements
========================

## Créer un nouvel évènement

Pour créer un événement, cliquez sur le bouton "Nouvel événement" ou, selon la vue choisie (jour, semaine, mois), cliquez sur le jour ou le créneau horaire souhaité dans la vue principale.

Une boîte de dialogue apparaît, vous pouvez y indiquer :

![Fenêtre nouvel événement](https://ahp.li/5738132457be63fcb23f.png)

1.  Le nom de l'événement
2.  L'agenda auquel l'associer
3.  Les heures de début et de fin et le fuseau horaire. Par défaut, la
    durée de l'événement est de :
    -   30 minutes pour les vues "jour" et "semaine".
    -   Toute la journée pour la vue "mois". Dans ce cas, décocher
        "Toute la journée" pour pouvoir modifier les heures de début et
        de fin.
4.  Le lieu
5.  La description

Si ces options suffisent, cliquez sur "Enregistrer".

### Options avancées

Pour des **options avancées**, cliquez sur "Plus". La barre latérale s'ouvre sur la droite de l'écran. En plus des options précédentes, vous pouvez :

**Dans l'onglet "Détails"**

![Onglet détails](https://ahp.li/63737c842534bd64c67d.png)

![Onglet Détails](https://ahp.li/d5cb173e9dca554ff23c.png)

1.  Indiquer le statut de l'événement : "*confirmé*", "*provisoire*" ou "*annulé*".
2.  Indiquer la visibilité de cet événement précis en cas d'agenda
    partagé : "*afficher l'événement entier*", "*afficher seulement
    comme occupé*", "*masquer l'événement*".
3.  Ajouter une Catégories (Anniversaire, Voyage, Réunion, ce que vous voulez)    
4.  Prévoir un rappel
5.  Configurer une récurrence : "*jamais*", "*tous les jours*", "*toutes les semaines*", "*tous les mois*" ou "*tous les ans*", avec ou sans date de fin.

**Dans l'onglet "Participants"**

![Onglet participants](https://ahp.li/7b5c578a9297c1a19c10.png)

1.  Utilisez le champ de saisie pour rechercher dans l'annuaire ou dans vos contacts privés le nom des participant⋅es à ajouter. Un email d'invitation est automatiquement envoyé. Par la suite, en cliquant sur le menu d'action associé à chaque nom, vous pouvez :

    ![Ajout participants événement](https://ahp.li/8b55fe14e1e104e350a1.png)

    -   Envoyer une nouvelle invitation par email à chaque modification de l'événement en cochant la case "Envoyer par courriel".
    -   Modifier le statut de chaque participant⋅e ("*président*",  "*obligatoire*", "*facultatif*", "*ne participe pas*") voire le⋅a supprimer de la liste.

2.  Le bouton "Créer une salle de discussion pour cet événement" ajoute un lien vers le module Discussion dans la description de l'événement.

> [!NOTE] 
> **L'onglet ressource** est utile en cas de gestion de réservation de salles avec Nextcloud et n'est pas encore documenté.

Cliquez sur "Enregistrer" ou "Mettre à jour" pour valider vos choix.

## Supprimer un événement
----------------------

Pour supprimer un évènement, cliquez dessus. Une boîte de dialogue apparaît.

Cliquez sur le menu d'actions en haut à droite de cette fenêtre.
Sélectionnez "Supprimer" dans le menu déroulant.

![Supprimer événement](https://ahp.li/f564c4d7bb3eee812ed3.png)

Pour retrouver un événement supprimé, cliquez sur la corbeille en bas à gauche de la vue générale.

![Corbeille](https://ahp.li/1a4e9b664c6cf77d0aa9.png)

Vous pouvez dans les actions (trois petits points) :

-   Restaurer l'événement
-   Supprimer définitivement l'événement.
-   Vider la corbeille

> [!WARNING]
> Les événements mis dans la corbeille sont automatiquement supprimés **définitivement** 30 jours après.