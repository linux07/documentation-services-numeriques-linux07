# Gérer ses agendas
=================

## Créer un nouvel agenda

Le module comporte par défaut un agenda intitulé "Personnel". Si vous avez besoin d'en créer un nouveau :

1.  Cliquez sur "Nouvel agenda" dans la liste des agendas. Dans le menu déroulant, cliquez sur l'option qui vous convient :

    ![Nouvel Agenda](https://ahp.li/0e07a741ef970fdf1787.png)

    -   *Nouvel agenda* : pour ajouter un agenda
    -   *Nouvel agenda avec liste de tâches* : pour créer un deck
        associé au nouvel agenda
    -   *Nouvel abonnement par lien* : pour intégrer un [agenda
        public](<../7. Agenda/7.3. Partager un agenda.md>) dans la liste des agendas

2.  Entrez un nom pour votre agenda et cliquez sur la flèche à droite pour valider.

    ![Nom Agenda](https://ahp.li/12cfa0ca910bd66762b6.png)

3.  Le nouvel agenda apparaît dans la liste.

## Éditer, Télécharger ou Supprimer un Agenda

Les agendas apparaissent tous dans la même vue, chacun avec leur **couleur associée**. Vous pouvez désactiver l'affichage de chaque
agenda individuellement en cliquant sur sa pastille de couleur, à gauche du nom, dans la liste.

Le menu d'actions à droite de chaque agenda, permet de :

![Action Agenda](https://ahp.li/b3643c7a94bdb44744e4.png)

-   *Modifier le nom* : renommer l'agenda
-   *Modifier la couleur* : changer la couleur d'affichage de l'agenda
-   *Copier le lien privé* : copier le lien de l'agenda pour les
    personnes y ayant accès
-   *Télécharger* : télécharger l'agenda au format .ics
-   *Supprimer* : supprimer l'agenda

> [!NOTE] 
> Vous pouvez retrouver vos agendas supprimés dans la corbeille
> en bas à gauche de la vue générale et les restaurer pendant 30 jours.

## Importer un agenda

Vous pouvez **importer un agenda** et ses évènements depuis un autre
logiciel :

1.  Cliquez sur "Paramètres & Importation" en bas à gauche de l'écran.

    ![Paramètes Ipmportation](https://ahp.li/27869ba81aa9da68559b.png)

2.  Cliquez sur "Importer un agenda".

    ![Importer un agenda](https://ahp.li/3890ef1901eaf45f8191.png)

3.  Sélectionnez un ou plusieurs fichiers de calendrier sur votre
    ordinateur (fichiers avec l'**extension .ics**) et cliquez sur
    "Ouvrir".

4.  Dans la boîte de dialogue qui s'ouvre, cliquez sur "Select option" pour définir dans quel agenda effectuer l'import :

    ![Select Option](https://ahp.li/48464a6a18024010c088.png)

    -   Vous pouvez ajouter l'agenda à un agenda déjà existant.
    -   Ou le conserver à part en cliquant sur "Nouvel agenda".

5.  Cliquez sur "Importer un agenda".

    ![Valid Import](https://ahp.li/d5435474d4fa21158a34.png)

> [!WARNING] 
> Cette action permet de **créer une copie**, mais pas de
> synchroniser des agendas. Toute nouvelle action sur l'agenda d'origine ne sera donc pas reportée sur l'agenda importé et inversement. 
> Pour synchroniser des agendas, voir la fiche [Synchroniser ses contacts et son agenda](<../nextcloud-mobile/synchroniser-contacts-agenda.md>).
