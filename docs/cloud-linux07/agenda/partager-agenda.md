# Partager un agenda
==================

## Partage interne
---------------

Vous pouvez **partager votre agenda** avec d'autres **utilisateur⋅ices ou cercles** (l'interface utilise le terme "groupes") de votre instance Nextcloud.

Cliquez sur l'icône de partage au bout de la ligne de l'agenda que vous souhaitez partager.

![Partage agenda](https://ahp.li/743787f58288ef88e2e5.png)

Vous pouvez autoriser la modification de l'agenda (création, modification, suppression d'événements) en cochant la case "peut modifier" associée à chaque destinataire du partage. Si la case n'est pas cochée, seule la consultation de l'agenda est possible.

![Autorisation modification agenda](https://ahp.li/61ab6077713a7b0715a7.png)

Vous pouvez arrêter de partager l'agenda en cliquant sur la corbeille en bout de ligne de chaque destinataire.

> [!NOTE] 
> Les agendas partagés avec vous apparaissent dans la liste au même niveau que les vôtres. 
> Ils se distinguent par la présence de l'avatar de leur propriétaire à la place du bouton de partage.

Pour supprimer un agenda qui vous a été partagé, cliquez sur le menu d'action puis sur "Cesser le partage de ma part". Vous avez quelques secondes pour changer d'avis en cliquant sur la flèche circulaire qui apparaît.
> [!Warning]
> Cette action n'est pas possible si le partage est fait pour un groupe dont vous faites partie.

![Cesser partage](https://ahp.li/63ea3235ed9a2f0997f3.png)

## Publier à l'extérieur
---------------------

Les agendas peuvent être **publiés** via un lien de partage pour les rendre accessibles (en lecture seule) à des **personnes externes** (non inscrites sur votre instance Nextcloud).

1.  Cliquez sur l'icône de partage de l'agenda à publier.

    ![Partage agenda](https://ahp.li/743787f58288ef88e2e5.png)

2.  Cliquez sur le bouton "**+**" au bout de la ligne "Lien de partage".

    ![Lien publication](https://ahp.li/07a9629a8f89887c5c9e.png)

3.  Cliquez sur l'icône de copie du lien public. Le lien est copié
    automatiquement dans votre presse-papier.

    ![Copier lien](https://ahp.li/0701be3101741949ce5f.png)

4.  Transmettez le liens aux destinataires de la manière de votre choix.

Les personnes ayant accès à ce lien ont la possibilité d'**ajouter l'agenda** à leur logiciel de calendrier personnel et de **télécharger** la totalité de l'agenda au format .ics

<figure>
<img src="acces-agenda.png" alt="Vue de l'agenda partagé côté destinataire" />
<figcaption><i>Côté destinataire, l'interface de l'agenda partagé est semblable à la vôtre, sans le bouton "Nouvel événement". Le menu d'action correspondant à l'agenda ne propose que les options "Copier le lien d'inscription" et "Télécharger".</i></figcaption>
</figure>

> [!WARNING] 
> Ce lien est **public** et peut circuler partout, à moins que vous ne révoquiez son partage.
>
> ![Option "supprimer le lien de partage" dans le menu d'actions du lien de partage.](https://ahp.li/7759eae4a294515883d2.png)

=================================================================
> [!TIP]
> Si vous êtes dans le besoin avec votre association d'avoir un calendrier partagé et éditable, il est possible sur notre instance Cloud Linux07 d'ajouter des utilisateurs avec très peu de quota dans un groupe isolé qui sera partagé avec votre groupe, cela leur permettra d'éditer un calendrier partagé.