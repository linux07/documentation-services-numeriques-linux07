L'installation de l'application Nextcloud Notes se fait de la même
manière sur **iPhone** et sur **Android**.

Elle necessite que soit déjà installé l'application Nextcloud

1.  Recherchez l'application "Nextcloud Notes" dans votre magasin
    habituel d'applications (se trouve aussi sur le dépôt F-Droid).

    ![Application Nextcloud Notes](https://ahp.li/b7ac202eafeda57dbba3.png)

2.  Installez l'application puis ouvrez-la.

3.  Choisissez l'adresse de connexion au serveur Nextcloud voulu.

4.  Appuyez sur "Se connecter" sur la page de votre collectif (les captures sont avec une autre instance, mais les principes sont les mêmes).

    <p><img src="https://ahp.li/572764cbbb9453635bda.png" alt="" width="300"/></p>

5.  Entrez le nom d'utilisateur et le mot de passe utilisés pour cet espace et cliquez sur "Se connecter".

    <p><img src="https://ahp.li/99121fb87c55e9ab58b1.png" alt="" width="300"/></p>

6.  Appuyez sur "Autoriser l'accès".

    <p><img src="https://ahp.li/c9e9de4468c3e9fc43db.png" alt="cautoriser nextcloud" width="300"/></p>

   
Vous avez désormais accès à vos notes Nextcloud depuis votre
mobile. Vous pouvez y réaliser les mêmes actions que sur l'interface
Web.

