# Gérer ses fichiers sur mobile

## Installer l'application Nextcloud

L'installation de l'application Nextcloud se fait de la même manière sur
**iPhone** et sur **Android**.

1.  Recherchez l'application "Nextcloud" dans votre magasin habituel
    d'applications.

    ![](https://ahp.li/bc0ff08e85cf00bcb6aa.png)

2.  Installez l'application puis ouvrez-la.

3.  Appuyez sur "Se connecter"

    <p><img src="https://ahp.li/d78093d9b979c2c431ad.png" alt="" width="300"/></p>

4.  **Ajouter un mot de passe d'application**

    En cas de perte ou de vol de votre mobile, il peut être utile de
    **révoquer à distance l'accès à votre espace**. Pour cela, il faut
    avoir créé un mot de passe d'application au moment de
    l'installation.

    1.  Posez votre téléphone et connectez vous à Nextcloud **depuis
        votre ordinateur.**

    2.  Ouvrez les paramètres.

        -   Voir : [1.2. Personnaliser les
            paramètres](<../premiers-pas/personnaliser-parametres.md>).

    3.  Cliquez sur "Sécurité" dans le menu à gauche.

    4.  Cliquez sur "Créer un nouveau mot de passe d'application" dans la section "Appareils et sessions".

        ![](https://ahp.li/2ea43305b68b8624a493.png)

    5.  Cliquez sur "Afficher le QR code pour les applications mobiles", sous les identifiants qui viennent d'être générés.

        ![](https://ahp.li/2ec4be473bcd4be70db4.png)

    6.  Retournez sur votre mobile et appuyez sur l'icône représentant
        un QR code, en bas de l'écran.

        <p><img src="https://ahp.li/8de8c929a8e297a89791.png" alt="" width="300" /></p>

    7.  Scannez le QR code de votre ordinateur avec votre mobile.

5.  Appuyez sur "Se connecter" sur la page de votre collectif qui vient de charger (les captures d'écran sont avec une autre instance Nextcloud, mais le principe est le même).

    <p><img src="https://ahp.li/572764cbbb9453635bda.png" alt="" width="300"/></p>

6.  Entrez l'adresse mail et le mot de passe utilisés pour cet espace et cliquez sur "Se connecter".

    <p><img src="chttps://ahp.li/99121fb87c55e9ab58b1.png" alt="" width="300"/></p>

7.  Appuyez sur "Autoriser l'accès".

    <p><img src="https://ahp.li/c9e9de4468c3e9fc43db.png" alt="" width="300"/></p>

8.  Appuyez sur "Autoriser" dans la boîte de dialogue qui aparaît, pour que l'application puisse bien accéder à
    vos fichiers sur votre mobile.

    <p><img src="https://ahp.li/6f629d96cef7cae1540e.png" alt="" width="300"/></p>

Vous avez désormais accès à votre espace Nextcloud depuis votre mobile.
Vous pouvez y réaliser les mêmes actions que sur l'interface Web.


## Envoyer et télécharger des fichiers

### Sur Android

#### Envoyer un fichier depuis votre téléphone vers Nextcloud

##### À partir de la liste de vos fichiers

1. Faites un appui long sur le nom du fichier.
2. Appuyez sur l'icône de partage de ce fichier.

   <p><img src="https://ahp.li/46c2c9dd445590d2c64a.png" alt="" width="300"/></p>
3. Sélectionnez "Nextcloud"
4. Choisissez le dossier de destination.
5. Appuyez sur "Envoyer".

   <p><img src="https://ahp.li/f9034bf3d7de96d4fc38.png" alt="" width="300"/></p>

##### À partir de l'application Nextcloud

1. Placez-vous dans le dossier de destination.
2. Appuyez sur le bouton **+** en bas à droite de l'écran.

    <p><img src="https://ahp.li/b7cac301ca49f53dbae5.png" alt="" width="300"/></p>
3. Appuyez sur "Téléverser des fichiers" dans la boîte de dialogue.

   <p><img src="https://ahp.li/6433b1c331c60613663b.png" alt="" width="300"/></p>
4. Sélectionnez le fichier à envoyer.
5. Appuyez sur "Envoyer".

   <p><img src="https://ahp.li/f9034bf3d7de96d4fc38.png" alt="" width="300"/></p>

#### Télécharger un fichier depuis Nextcloud vers votre téléphone

1. Appuyez sur le menu d'actions du fichier concerné
2. Appuyez sur "Télécharger" dans le menu déroulant.

   <p><img src="https://ahp.li/6ae5f6eeed8da05f2a8a.png" alt="" width="300"/></p>

### Sur iPhone

🚧 Documentation à venir 🚧

## Révoquer l'accès d'un appareil

Si vous changez de téléphone, ou que vous le perdez ou qu'on vous le vole, vous pouvez révoquer l'accès à votre espace depuis l'interface web.

1. Ouvrez les paramètres
-   Voir : [1.2. Personnaliser les paramètres](<../premiers-pas/personnaliser-parametres.md>).
2. Cliquez sur "Sécurité" dans le menu de gauche
3. Cliquez sur le menu d'action correspondant à l'appareil dont vous souhaitez révoquer l'accès, dans la section "Appareils et sessions".

   ![revoquer-acces-appareil.png](https://ahp.li/a44a5186df0aa4db5024.png)
4. Cliquez sur "Révoquer" dans le menu déroulant.