# Nextcloud sur mobile

Nextcloud permet la synchronisation de plusieurs de ses modules sur mobile (Android et iOS) :

* Fichiers
* Discussion
* Contacts et Agenda
* Notes
* Tâches

Pour chaque module, une application différente est nécessaire.

> [!TIP]
> Sur un Android, vous pouvez utiliser un autre dépôt que le PlayStore de Google. Je recommande le dépôt **F-Droid**. Plusieurs applications libres sont dans ce dépôt et parfois certaines applications payantes sur le PlayStore seront gratuites sur ce dépôt.
> F-Droid, en dehors d'être un dépôt d’applications Android, propose également un client Android permettant d'installer facilement ces applications et de les mettre à jour. Pour installer l’application F-Droid, l'utilisateur doit cocher l'option permettant l’installation d'applications depuis des sources inconnues dans ces paramètres, et avoir au préalable téléchargé l'apk depuis le site officiel avec un navigateur internet classique.
[Téléchargement F-Droid](https://f-droid.org/fr/)
