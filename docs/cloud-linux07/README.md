# Cloud Linux07

Le  **Cloud Linux07** est un service pour **Stocker**,**organiser**, **partager** et **sécuriser** toutes sortes de documents est désormais indispensable pour de nombreux utilisateurs. Le Cloud fournit une solution flexible grâce à laquelle il est possible d’avoir accès à ces fichiers depuis n’importe quel appareil connecté. Si le stockage sur le Cloud apporte une réponse concrète à un besoin bien réel, les services proposant diverses versions et interfaces pour ce type de stockage sont légion.

Ce service utilise le logiciel libre [Nextcloud](https://fr.wikipedia.org/wiki/Nextcloud/). Le logiciel **Nextcloud** comporte ainsi ses propres spécificités. 
Sur celui-ci nous avons ajouter ces extensions 
* [Contacts](https://apps.nextcloud.com/apps/contacts) pour gérer ses contacts, 
* [Agenda](https://apps.nextcloud.com/apps/calendar) pour créer des Agendas, 
* [Talk](https://apps.nextcloud.com/apps/spreed) pour créer des discussions,
* [Notes](https://apps.nextcloud.com/apps/notes) pour créer des notes, 
* [Collectives](https://apps.nextcloud.com/apps/collectives) pour créer des pages collaboratives, 
* [Formulaires](https://apps.nextcloud.com/apps/forms) pour créer des formulaires, 
* [Tâches](https://apps.nextcloud.com/apps/tasks) pour créer des tâches, 
* [Onlyoffice](https://apps.nextcloud.com/apps/onlyoffice) pour l'édition collaborative 
* OnlyOffice est maintenant connecté sur un serveur installé sur le Yunohost (sur le même serveur que les services Linux07).

Toutes ces applications et les données peuvent être partagées et/ou synchronisées avec des applications clientes sur un ordinateur de bureau et/ou un mobile.