## Créer une **discussion privée**

1.  Cherchez le nom de la personne à qui vous voulez parler dans le champ de recherche à gauche de la page et cliquez
    dessus.

    ![](chttps://ahp.li/0ffe6d1bff9b4fbca4ef.png)

2.  La discussion est immédiatement créée et apparaît dans la vue principale.

## Créer une **discussion de groupe**

1.  Cliquez sur le bouton **+** à côté du champ de recherche à gauche de la page.

    ![](https://ahp.li/44433449a11ea222ac78.png)

2.  Donnez un nom à la discussion dans la boîte de dialogue qui apparaît.
3. Sélectionnez les paramètres souhaités
   * Autoriser les invités à rejoindre la conversation avec le lien -> pour inviter des personnes non inscrites sur l'instance.
     * Ajoutez éventuellement un mot de passe.
   * Ouvrir la conversations aux utilisateurs enregistrés -> pour rendre la discussion publique et permettre aux membres de l'instance de s'y joindre spontanément.

     ![](https://ahp.li/e6befffc3fc8541f56ab.png)
4. Cliquez sur Ajouter des participants.
5. Une nouvelle vue apparaît. Recherchez les membres, groupes ou cercles que vous souhaitez ajouter.
   * Utilisez la barre de défilement pour accéder aux groupes existants

     ![](https://ahp.li/6cfceb5bebc2f443255c.png)
6. Cliquez sur Créer une conversation.

> [!NOTE]
> Pour annuler la création d'une conversation, cliquez en dehors de la boîte de dialogue.

## Gérer les rôles

Quand vous créez une discussion de groupe, vous en êtes automatiquement **modérateur⋅ice**.


Pour accorder ce droit aux autres membres de la discussion :
1. Ouvrez l'onglet d'informations de la discussion avec l'icône sandwich en haut à droite
![](https://ahp.li/b5df5255ed71ef570a57.png)

2. Cliquez sur le menu d'actions à droite de leur nom
3. Choisissez "Promouvoir en modérateur" dans le menu déroulant.

![](https://ahp.li/2a26370e2ec62ed886c5.png)

Vous pouvez aussi retirer des membres de la discussion.

Les modérateur⋅ices peuvent **modifier les paramètres** de la discussion (nom, accès, description, mais aussi suppression).