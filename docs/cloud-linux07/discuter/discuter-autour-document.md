## Partager un document

Il y a deux façons de partager un document dans une conversation.

* Cliquez-glissez le document directement dans la conversation.
* Cliquez sur le trombone à gauche de la zone d'écriture.

  ![](https://ahp.li/6b7c44b7bec71e707b03.png)
   Le menu déroulant propose :
  * Envoyer de nouveaux fichiers -> depuis votre ordinateur.
  * Partager vos fichiers déjà stockés -> depuis votre espace personnel sur Nextcloud.

Vous pouvez ajouter plusieurs fichiers en une seule fois.

Tou⋅tes les membres de la discussion pourront **voir, éditer ou télécharger** le document, qu'ils ou elles soient inscrit⋅es sur l'instance ou venant de l'extérieur.

## Lier une discussion à un document

Dans le module **Fichiers**, vous pouvez démarrer une discussion écrite ou vidéo **depuis la barre latérale** de chaque document pendant que vous l'éditez.

1. Ouvrez un document.
2. Cliquer sur le menu d'actions en haut à droite de la page pour ouvrir la barre latérale.

   ![](https://ahp.li/2af92a1f532f916af935.png)

3. Cliquez sur l'onglet "Tchat".

4. Cliquez sur le bouton "Partager ce fichier".

 ![](https://ahp.li/9992917254bcc2a886a0.png)

5. Dans le champ de recherche en tapant les premiers mots du nom de la conversation, elle apparaît, vous pouvez cliquez sur la conversation que vous voulez rejoindre.

![](https://ahp.li/04af007450a057cdcc17.png)   

6. Tapez votre message dans la zone d'écriture ou cliquez sur "Commencer l'appel".

Vous pouvez aussi rejoindre la discussion **depuis la liste des documents** :

1. Cliquez sur le menu de partage pour ouvrir la barre latérale ou actions (trois points de suspenssion) et infos.

   ![](https://ahp.li/770c0df87dff133dbab3.png)
2. Cliquez sur l'onglet "Tchat"
3. Cliquez sur le bouton "Partager ce fichier"

   ![](https://ahp.li/ea90b0cd0f6400c036b4.png)

5. Dans le champ de recherche en tapant les premiers mots du nom de la conversation, elle apparaît, vous pouvez cliquez sur la conversation que vous voulez rejoindre.

![](https://ahp.li/be93f79dc33adac7c4e8.png)   

4. Tapez votre message dans la zone d'écriture ou cliquez sur "Commencer l'appel".

La conversation ainsi créée apparaît dans la **liste du module Discussion**. Vous pouvez y ajuster les paramètres en cliquant sur le menu d'action correspondant.

![](https://ahp.li/63305057549f893d1350.png)

============================================================================