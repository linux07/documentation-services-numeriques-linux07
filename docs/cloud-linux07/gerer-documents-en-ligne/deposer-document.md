# Déposer un document

-   Cliquez sur le bouton **+** en haut de la vue principale pour ouvrir le menu.

-   Choisissez "Envoyer un fichier" dans le menu déroulant.

    ![Envoyer fichier](https://ahp.li/fece1d6c6eee181c8dec.png)

-   La fenêtre "Ouvrir un fichier" de votre navigateur apparaît et vous
    pouvez ainsi parcourir vos données locales (sur votre ordinateur).
    Sélectionnez le ou les fichiers (Maintenez la touche Ctrl enfoncée
    pour sélectionner plusieurs fichiers) puis validez.

-   Patientez pendant l'envoi.

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=07266f77f9a2ebe11bf9d21354c08976f1420d6d"></iframe>

Déposer par cliquer-glisser
---------------------------
<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=dd313f7099816d3206595e10b0a5c118f4996715"></iframe>
