# Gérer ses documents en ligne

Le module **Fichiers** permet d'organiser ses documents et de les partager avec d'autres personnes en interne ou à l'extérieur.

Tour d'horizon de l'interface :

![Fichiers](https://ahp.li/7e0fddff9d3debb3d302.png)

1. Le menu de gauche permet de **filtrer l'affichage**. "Tous les fichiers" permet de parcourir l'arborescence générale.
2. Le contenu du dossier courant est affiché par défaut sous forme de liste dans la vue principale.
    -   La case à cocher en tête de ligne permet de sélectionner un (ou
        plusieurs) document ou dossier.
    -   Cliquez sur le nom d'un dossier ou d'un document pour l'ouvrir.
    -   En fin de ligne se trouvent deux boutons. Le premier pour
        **partager le document**, le second (trois points) pour ouvrir un
        menu d'actions.
    -   La taille (poids) et la date de dernière modification du fichier
        sont affichés ensuite.
3.  Le bouton **+** en haut de la vue principale permet de **déposer des documents**, de **créer un nouveau
    dossier** ou un **nouveau fichier** (document en docx, feuille de calcul, présentation, nouveau lien, modèle de formulaire).
4.  En bas à gauche de la page se trouvent :
    -   l'accès à la **corbeille** ;
    -   l'état d'occupation de votre espace en fonction de votre quota ;
    -   un menu de paramètres (spécifique à chaque module).
