# Restaurer une ancienne version d’un document

Nextcloud **sauvegarde des versions successives d'un même document**. Cette fonction est utile par exemple pour retrouver une copie d'un document en cas d'erreur d'édition.

Pour **restaurer une ancienne version** d'un document :

1.  Cliquer sur le choix des actions du document sur la ligne (trois petits points).
2.  Choisissez **i Détails** pour voir s'ouvrir la colonne des détails du fichier.

    ![Choix fichier](https://ahp.li/31f4f3110d543318ff4f.png)

3.  Cliquez sur l'onglet "Versions".

4.  Cliquez sur la flèche circulaire en bout de ligne de la version que vous
    souhaitez restaurer.

    ![Détail fichier versions](https://ahp.li/9a119e0e9ea50ad0eb9d.png)