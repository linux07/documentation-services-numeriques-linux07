# Créer un nouveau dossier

## D'un coup d’œil

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=db3aa878c9c91445e6078b7ea2c4f078d0eab126"></iframe>

## Étape par étape

1.  Cliquez sur le bouton **+** en haut de la vue principale pour ouvrir le menu.

    ![Nouveau dossier](https://ahp.li/7e5823e54fcef5936bee.png)

2.  Choisissez "Nouveau dossier" dans le menu déroulant.

    -   Définissez un nom.

    -   Validez en cliquant sur la flèche ou avec la touche "Entrée" du
        clavier.

    ![Définir nom](https://ahp.li/7c0ef4ece8ca28cd8a79.png)

3.  Le dossier apparaît dans la liste ainsi que sa fenêtre de détails à droite.

    ![Panneau information](https://ahp.li/030a17de3e7778910979.png)

4.  Cliquez sur le nom du nouveau dossier dans la liste pour l'ouvrir.