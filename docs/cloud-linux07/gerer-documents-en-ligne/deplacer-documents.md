# Déplacer des documents

### D'un coup d'œil

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=d016cb4fa3ef540e24b105ca0cd3131a504508f7"></iframe>

### Étape par étape

1.  Cochez les fichiers à copier ou déplacer dans la liste des dossiers.

2.  Ouvrez le menu "Actions" dont le bouton apparaît en haut de la liste dès qu'un fichier
    ou dossier est sélectionné.

3.  Choisissez "Déplacer ou copier" dans le menu déroulant.

    ![Déplacer plusieurs fichiers menu](https://ahp.li/9c2c8bf70e0ccc12837e.png)

    -   Vous pouvez ensuite parcourir l'arborescence de votre espace
        Nextcloud (l'icône « maison » représente la racine de votre
        dossier personnel).

4.  Choisissez le dossier de destination.

5.  Choisissez de copier (le document reste présent dans le dossier
    d'origine) ou de déplacer (le document est supprimé du dossier
    d'origine).

6.  Patientez quelques secondes.

Déplacer par cliquer-glisser
----------------------------
<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=e293658f192f9c114a289b28be57f840bde9e08b"></iframe>

1.  Cochez les fichiers à copier ou déplacer dans la liste des dossiers.
2.  Cliquez sur l'un des fichiers puis déplacez la souris sans lâcher le
    bouton gauche. Déposez les fichiers sur le dossier de votre choix.