# Supprimer et restaurer des fichiers

## Supprimer des fichiers

1.  Cochez les fichiers à supprimer dans la liste des dossiers.

2.  Ouvrez le menu "Actions" dont le bouton apparaît en haut de la liste dès qu'un fichier
    ou dossier est sélectionné.

3.  Choisissez "Supprimer" dans le menu déroulant.

    ![Supprimer fichiers](https://ahp.li/785432fc159cf8b69f17.png)

Restaurer des fichiers supprimés
--------------------------------

1.  Cliquez sur "Fichiers supprimés" en bas à gauche de l'interface.

2.  Cochez les fichiers à restaurer dans la liste.

3.  Ouvrez le menu "Actions" dont le bouton apparaît en haut de la litste dès qu'un fichier
    ou dossier est sélectionné.

4.  Choisissez "Restaurer" dans le menu déroulant.

    ![Restaurer fichiers](https://ahp.li/70599c94c7d3cdd68e8c.png)