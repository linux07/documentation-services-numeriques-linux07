En vous connectant vous arrivez sur le **Tableau de Bord**, en scrollant en bas, vous pouvez le personnaliser.

![Peronnaliser tableau de bord](https://ahp.li/1d2cd2e50901890f3007.png "tableau-de-bord-personnaliser")

Choisir d'activer ou non des **Widgets**, blocs avec des informations, choisir votre ville pour la météo si vous gardez son Widget, changer l'image de fond...

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=bf59cc6ab840eb4741608392733f4b60df13bc28"></iframe>