# Personnaliser les paramètres

Cliquez sur l'icône ronde colorée portant vos initiales dans le coin supérieur droit.

![Choisir paramètres](chttps://ahp.li/a25fc43ce1ecb915a152.png)

Choisissez "Paramètres".

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=113096eb8f8b8050063dfa962bf4932a9c6aac60"></iframe>


Vérifiez vos informations personnelles.

<img src="https://ahp.li/ecfc0caceccfe6ecf747.png" width="640" height=480 />

1. Une adresse email est nécessaire pour réinitialiser votre mot de passe et éventuellement recevoir des notifications. Les autres informations sont facultatives.
2. Vous pouvez déposer une photo qui sera affichée sous forme de pastille dans certains modules, comme les discussions (*chat*) ou les partages de documents. Vous pouvez également utiliser une image déjà présente dans votre espace.
3. Assurez-vous que la langue et les paramètres régionaux (heure locale, début de semaine) sont bons.
4. Vous pouvez ajoutez des informations qui apparaîtront sur votre profil.

   ![Paramètres profil](https://ahp.li/45b5d201f11466505154.png)

L'enregistrement des paramètres est automatique, vous pouvez reprendre vos activités.

> [!WARNING]
> **Attention à votre bloqueur de publicités** Si vous utilisez un bloqueur de publicité, il peut bloquer certaines fonctionnalités de Nextcloud selon les filtres que vous avez choisis. Si c'est le cas, vous pouvez désactiver votre bloqueur de publicités pour l'adresse de votre espace Nextcloud (et seulement pour cette adresse).