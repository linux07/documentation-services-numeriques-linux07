Pour utiliser Cloud Linux07 sur un navigateur web, entrez directement l’url de l'espace que vous souhaitez rejoindre dans la barre d'adresse de votre navigateur. En général, elle se présente sous la forme <https://nc.linux07.fr>.

> [!TIP]
> Pour les adhérents à Linux07, vous pouvez vous connecter directement au [portail Linux07](https://linux07.fr)	pour accéder à tous vos services, vous pourrez entrer dans le Cloud Linux07 sans avoir besoin de vous identifier de nouveau.

Sur cet espace que vous rejoignez:
* soit vous devez créer un compte vous-même : suivez les étapes indiquées en cliquant sur **S'inscrire**, le compte doit être validé par un administrateur par la suite, pour cela vous devez aussi nous en faire la demande en nous contactant. Le compte sera validé après un échange et que si nous avons assez d'espace. Un tutoriel est mis à disposition ici: [Tutoriel inscription Cloud Linux07](https://nc.linux07.fr/s/kj5kCoat5sE96cJ)
* Soit un identifiant et un mot de passe ont été déjà enregistrés et votre compte est validé : entrez-les dans les champs correspondants. 


> [!WARNING]
> Si vous êtes sur un ordinateur public ou qui n’est pas le vôtre, assurez-vous de ne pas sauvegarder vos mots de passe dans le navigateur web. Si vous êtes sur votre ordinateur habituel, vous pouvez demander à votre navigateur ou à votre gestionnaire de mots de passe de s'en souvenir.