# Découvrir la page d'accueil

Lorsque vous vous connectez pour la première fois, vous êtes accueilli par une bannière de présentation de Nextcloud. Vous pouvez cliquer sur la flèche pour visionner les 3 diapositives ou cliquer sur la croix pour la fermer.

<img src="https://ahp.li/1ec0b7dfa7d1ff5ea506.png" width="640" height=360 />

La page d'accueil est celle de vos dossiers et contenus.

<img src="https://ahp.li/2ddd9be5bd25911f309b.png" width="640" height=360 />

La vue principale montre les dossiers et fichiers disponibles sur l'espace. À gauche, un menu de navigation. En haut, une barre d'accès aux différent modules, à gauche, et à la recherche et au profil, à droite.

Repérez les icônes des modules disponibles. Passez la souris sur la barre d'icônes pour voir leur nom apparaître.

<iframe width="660" height="452" frameborder="0" allowfullscreen src="https://www.archive-host.com/media/videos/jwplayer/index.php?id=c7a2ff424b5baed4ab654cdd516b73afd7ed8244"></iframe>


> [!NOTE]
> Depuis la version 22 de Nextcloud, en vous connectant vous arriverez sur votre **Tableau de Bord** et non plus sur la page des contenus **Fichiers**.