# Collectifs

Le module Collectifs permet de créer des groupes de travail autour de **documents partagés**, éditables en commun. 

Il est aussi possible d'y associer un Agenda (voir fiche Agenda) ou un Carnet d'adresse. (voir fiche Contacts)...
