# Créer des pages

## Créer une nouvelle page

* Cliquez sur le bouton **+** à côté du nom de votre collectif dans le menu de gauche.

  ![](https://ahp.li/cd88d986fb0cc4acd97f.png)
* Une nouvelle page apparaît dans la liste.
* Nommez la page dans le champ texte en haut de la vue principale.

  ![](https://ahp.li/1bcb1c381939198ab2d9.png)
* L'enregistrement est automatique.
* Chaque page a son propre bouton **+** qui permet de créer une **arborescence** dans laquelle organiser le travail.
* Les pages sont organisées par date de dernière modification ou par ordre alphabétique. Pour changer l'affichage, cliquez sur le bouton en haut de l'arborescence.

  ![](https://ahp.li/dd7efecd4631e14570f9.png)

## Modifier une page

1. Cliquez sur la page que vous souhaitez modifier dans la liste.
2. Cliquez sur le bouton "Modifier" à droite du titre, en haut de la vue principale.

   ![](https://ahp.li/d238a4391833cdd72339.png)
3. L'enregistrement est automatique. Vous pouvez vérifier en regardant l'état affiché en haut de l'éditeur de texte.
   * "Enregistrement…"
   * "Enregistré"

     ![](https://ahp.li/bc6cb4ac9ed93be2ec4e.png)

     ![](https://ahp.li/a7c875c8cef8742f856a.png)
4. Quand vous avez terminé l'édition de la page, cliquez sur le bouton "Terminé" à droite du titre.

   ![](https://ahp.li/39099165b68c900c05f3.png)

L'ensemble des pages créées se retrouve dans un **dossier "Collectifs"** du module **Fichiers**, organisées en dossiers, sous-dossiers et documents texte.

![](https://ahp.li/cab114ad254a721021eb.png)

![](https://ahp.li/52ad2d1cf5bbf744f4b5.png)