# Créer un collectif

1. Cliquez sur le module **Collectifs** dans la barre des modules en haut à gauche.

   ![](https://ahp.li/c0c52c38be6592b6a624.png)
2. Cliquez sur "Créer un nouveau collectif" dans le menu à gauche.
3. Donnez un nom à votre collectif.
   * Vous pouvez ajouter un émoji pour le reconnaître rapidement en cliquant sur l'icône à gauche du champ de saisie.

     ![](https://ahp.li/387cc3dcbfb3eeebade4.png)
4. Cliquez sur la flèche à droite du champ de saisie pour valider.