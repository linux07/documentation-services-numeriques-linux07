# Ajouter des membres

1. Affichez la liste des collectifs en cliquant sur le bouton de menu à gauche

   ![](https://ahp.li/a8489529a4e48308a4a9.png)
2. Cliquez sur le menu d'action correspondant à ce collectif.

   ![](https://ahp.li/b0b392c692409b993899.png)
3. Sélectionnez "Gérer les membres" dans le menu déroulant.

   ![](https://ahp.li/aaa97e46a92dc4f72964.png)
4. Nextcloud renvoie automatiquement sur le module **Contacts**.
    Cliquez sur "Ajouter des membres" dans le menu à gauche de la vue principale.

    -   Le module créé automatiquement un **Cercle** du même nom que le
        collectif. C'est à celui-ci que les membres sont ajouté⋅es.

   ![](https://ahp.li/8d0f6a652dd441dcf355.png)

5. Dans la boîte de dialogue qui apparaît, commencez à taper le nom de membres à ajouter et cliquez dessus pour les sélectionner.

   ![](https://ahp.li/45eccdd5e0d32675284d.png)
6. Cliquez sur "Ajouter à \[nom de votre collectif\]".
   * Vous pouvez ajouter des contacts (inscrit⋅es sur votre instance), des groupes ou des cercles entiers.
   -   Pour gérer les rôles et les paramètres du Cercle (et par
        extension, du Collectif), voir la fiche Contacts...
