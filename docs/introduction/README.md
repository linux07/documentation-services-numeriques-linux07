# Introduction

## Documentation services numériques Linux07

* Découvrir les bases d'utilisation des services Linux07 pour ses heureux utilisateurs.
* Connaître l'infrastructure des services numériques Linux07
* Documenter les étapes techniques.

Cette documentation est librement inspirée de doucumentaions existantes, pour aider les utilisateurs des [services numérques Linux07](https://www.linux07.fr), utilisant des logiciels libres comme:
* Nextcloud   (Cloud Linux07)
* Rouncube    (Mails Linux07) 
* Mattermost  (Linux07 Teams)
* Mobilizon   (Mobilizon Linux07)
* Cryptpad    (Cryptpad Linux07)
* Opensondage (Linux07date)
* Mypads      (Mypads Linux07)
* Libreto     (Libreto Linux07)  

## Les services Linux07

### Qu'est-ce que c'est ?

Un ensemble d'Alternatives aux services propriétaires dit [GAFAM](https://fr.wikipedia.org/wiki/GAFAM) mis en place par une l'association [Linux07](https://linux07.aufildudoux.fr). 
Des services Libres sans aucune exploitation commerciale ou autres de vos données accessibles à cette adresse: [Services numériques Linux07](https://www.linux07.fr)

### Pourquoi ?

Notre souhait est de pouvoir continuer à **partager**, **échanger** et à **faire la promotion du Libre** autours de nous. Proposer ces services ont pour but de vous montrer des alternatives d'outils numériques ethiques possibles pour collaborer.
