Pour la sécurité en cas de besoins de restauration des données:

Nous avons des sauvegardes locales journalières des données et du système sur le  volume `/mnt/backups/borgarchives` de 1 To avec  avec [Borg App](https://github.com/YunoHost-Apps/borg_ynh), plus une sauvegarde journalière (aussi avec Borg App), chiffrée sur un serveur Yunohost auto-hébergé où est installé [Borg Server](https://github.com/YunoHost-Apps/borgserver_ynh) sur un disque dur de 2 To.

> [!WARNING]
> Il est tout de même conseillé malgré toutes ces précautions aux utilisateurs qui ont des données sur Nextcloud (Cloud Linux07) de bien faire des sauvegardes de temps à autres. Nous ne pouvons pas garantir à 100% de ne jamais rien perdre, mais nous faisons tout notre possible pour éviter de risquer de perdre vos données.


Fichier de configuration des sauvegardes distantes vers BorgServer (machine auto-hébergé à l'adresse de l'association Linux07)

**/etc/yunohost/hooks.d/backup_method/05-borg_app**

```
#!/bin/bash

set -eo pipefail
app="${0#"./05-"}"
app="${app%"_app"}"

BORG_PASSPHRASE="$(yunohost app setting $app passphrase)"
repo="$(yunohost app setting $app repository)"   #$4
if ssh-keygen -F "[domain.tld]:6060" >/dev/null ; then
    BORG_RSH="ssh -i /root/.ssh/id_${app}_ed25519 -oStrictHostKeyChecking=yes "
else
    BORG_RSH="ssh -i /root/.ssh/id_${app}_ed25519 -oStrictHostKeyChecking=no "
fi

do_need_mount() {
    true
}

LOGFILE=/var/log/backup_borg.err
log_with_timestamp() {
  sed -e "s/^/[$(date +"%Y-%m-%d_%H:%M:%S")] /" | tee -a $LOGFILE
}

do_backup() {
    
    export BORG_PASSPHRASE
    export BORG_RSH
    export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
    work_dir="$1"
    name="$2"
    repo="$3"
    size="$4"
    description="$5"
    current_date=$(date +"%Y-%m-%d_%H:%M")
    pushd "$work_dir"
    set +e
    if borg init -e repokey "$repo" ; then
	    #human_size=`echo $size | awk '{ suffix=" KMGT"; for(i=1; $1>1024 && i < length(suffix); i++) $1/=1024; print int($1) substr(suffix, i, 1), $3; }'`
	    # Speed in Kbps
	    #speed=1000
	    #evaluated_time=$(($size / ($speed * 1000 / 8) / 3600))
	    echo "Hello,

Your first backup on $repo is starting.

This is an automated message from your beloved YunoHost server." | /usr/bin/mail.mailutils -a "Content-Type: text/plain; charset=UTF-8" -s "[YNH] First backup is starting" "root"
    fi
    set -e

    borg create "$repo::_${name}-${current_date}" ./ 2>&1 >/dev/null | log_with_timestamp
    popd

    # About thi _20 it's a crazy fix to avoid pruning wordpress__2 
    # if you prune wordpress
    borg prune "$repo" -P "_${name}-" --keep-hourly 2 --keep-daily=7 --keep-weekly=8 --keep-monthly=12 2>&1 >/dev/null | log_with_timestamp
    
    # Prune legacy archive name without error on wordpress/wordpress__2
    borg prune "$repo" -P "${name}_" --keep-within 2m --keep-monthly=12 2>&1 >/dev/null | log_with_timestamp
    
    # We prune potential manual backup older than 1 year
    borg prune "$repo" --keep-within 1y 2>&1 >/dev/null | log_with_timestamp
}

do_mount() {
    export BORG_PASSPHRASE
    export BORG_RSH
    work_dir="$1"
    name="$2"
    repo="$3"
    size="$4"
    description="$5"
    borg mount "$repo::$name" "$work_dir" 2>&1 >/dev/null | log_with_timestamp
}

work_dir="$2"
name="$3"

size="$5"
description="$6"

case "$1" in
  need_mount)
    do_need_mount "$work_dir" "$name" "$repo" "$size" "$description"
    ;;
  backup)
    do_backup "$work_dir" "$name" "$repo" "$size" "$description"
    ;;
  mount)
    do_mount
    ;;
  *)
    echo "hook called with unknown argument \`$1'" >&2
    exit 1
    ;;
esac

exit 0
```

Fichier de configuration des sauvegardes locales

**/etc/yunohost/hooks.d/backup_method/05-borg__2_app**

```
#!/bin/bash

set -eo pipefail
app="${0#"./05-"}"
app="${app%"_app"}"

BORG_PASSPHRASE="$(yunohost app setting $app passphrase)"
repo="$(yunohost app setting $app repository)"   #$4
if ssh-keygen -F "" >/dev/null ; then
    BORG_RSH="ssh -i /root/.ssh/id_${app}_ed25519 -oStrictHostKeyChecking=yes "
else
    BORG_RSH="ssh -i /root/.ssh/id_${app}_ed25519 -oStrictHostKeyChecking=no "
fi

do_need_mount() {
    true
}

LOGFILE=/var/log/backup_borg.err
log_with_timestamp() {
  sed -e "s/^/[$(date +"%Y-%m-%d_%H:%M:%S")] /" | tee -a $LOGFILE
}

do_backup() {
    
    export BORG_PASSPHRASE
    export BORG_RSH
    export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
    work_dir="$1"
    name="$2"
    repo="$3"
    size="$4"
    description="$5"
    current_date=$(date +"%Y-%m-%d_%H:%M")
    pushd "$work_dir"
    set +e
    if borg init -e repokey "$repo" ; then
	    #human_size=`echo $size | awk '{ suffix=" KMGT"; for(i=1; $1>1024 && i < length(suffix); i++) $1/=1024; print int($1) substr(suffix, i, 1), $3; }'`
	    # Speed in Kbps
	    #speed=1000
	    #evaluated_time=$(($size / ($speed * 1000 / 8) / 3600))
	    echo "Hello,

Your first backup on $repo is starting.

This is an automated message from your beloved YunoHost server." | /usr/bin/mail.mailutils -a "Content-Type: text/plain; charset=UTF-8" -s "[YNH] First backup is starting" "root"
    fi
    set -e

    borg create "$repo::_${name}-${current_date}" ./ 2>&1 >/dev/null | log_with_timestamp
    popd

    # About thi _20 it's a crazy fix to avoid pruning wordpress__2 
    # if you prune wordpress
    borg prune "$repo" -P "_${name}-" --keep-hourly 2 --keep-daily=7 --keep-weekly=8 --keep-monthly=12 2>&1 >/dev/null | log_with_timestamp
    
    # Prune legacy archive name without error on wordpress/wordpress__2
    borg prune "$repo" -P "${name}_" --keep-within 2m --keep-monthly=12 2>&1 >/dev/null | log_with_timestamp
    
    # We prune potential manual backup older than 1 year
    borg prune "$repo" --keep-within 1y 2>&1 >/dev/null | log_with_timestamp
}

do_mount() {
    export BORG_PASSPHRASE
    export BORG_RSH
    work_dir="$1"
    name="$2"
    repo="$3"
    size="$4"
    description="$5"
    borg mount "$repo::$name" "$work_dir" 2>&1 >/dev/null | log_with_timestamp
}

work_dir="$2"
name="$3"

size="$5"
description="$6"

case "$1" in
  need_mount)
    do_need_mount "$work_dir" "$name" "$repo" "$size" "$description"
    ;;
  backup)
    do_backup "$work_dir" "$name" "$repo" "$size" "$description"
    ;;
  mount)
    do_mount
    ;;
  *)
    echo "hook called with unknown argument \`$1'" >&2
    exit 1
    ;;
esac

exit 0
```

