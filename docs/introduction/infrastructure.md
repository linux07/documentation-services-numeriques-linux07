Nous hébergeons ces services sur un serveur dédié Kimsufi avec 3 disques de 2 To montés en SoftRaid 1 (c'est à dire que le premier disque est dupliqué en miroir et synchronisé sur les 2 autres. Si le disque lâche où s'abîme, les données répliquées sur les autres disques peuvent prendre le relais) avec 32 Go de ram et 8 CPUs.


Informations détaillées sur le serveur:

```
System:
  Host: linux07.fr Kernel: 5.10.0-18-amd64 x86_64 bits: 64 Console: tty 0 
  Distro: Debian GNU/Linux 11 (bullseye) 
Machine:
  Type: Desktop Mobo: Intel model: DH67BL v: AAG10189-213 
  serial: <superuser required> BIOS: Intel 
  v: BLH6710H.86A.0163.2018.1023.1559 date: 10/23/2018 
CPU:
  Info: Quad Core model: Intel Xeon E3-1245 V2 bits: 64 type: MT MCP 
  L2 cache: 8 MiB 
  Speed: 1634 MHz min/max: 1600/3800 MHz Core speeds (MHz): 1: 1634 2: 1751 
  3: 1616 4: 1623 5: 1705 6: 2418 7: 1676 8: 1755 
Memory:
  RAM: total: 31.26 GiB used: 4.56 GiB (14.6%) 
  Array-1: capacity: 32 GiB slots: 4 EC: None 
  Device-1: CHANNEL A DIMM0 size: 8 GiB speed: 1600 MT/s 
  Device-2: CHANNEL A DIMM1 size: 8 GiB speed: 1600 MT/s 
  Device-3: CHANNEL B DIMM0 size: 8 GiB speed: 1600 MT/s 
  Device-4: CHANNEL B DIMM1 size: 8 GiB speed: 1600 MT/s 
Network:
  Device-1: Intel 82579V Gigabit Network driver: e1000e 
  IF: eno1 state: up speed: 1000 Mbps duplex: full mac: 4c:72:b9:d1:e3:fb 
RAID:
  Device-1: md1 type: mdraid level: mirror status: active size: 68.3 GiB 
  report: 3/3 UUU 
  Components: Online: 0: sda1 1: sdb1 2: sdc1 
  Device-2: md3 type: mdraid level: mirror status: active size: 1.75 TiB 
  report: 3/3 UUU 
  Components: Online: 0: sda3 1: sdb3 2: sdc3 
Drives:
  Local Storage: total: raw: 5.46 TiB usable: 1.82 TiB 
  used: 51.67 GiB (2.8%) 
  ID-1: /dev/sda vendor: HGST (Hitachi) model: HUS724020ALA640 
  size: 1.82 TiB 
  ID-2: /dev/sdb vendor: HGST (Hitachi) model: HUS724020ALA640 
  size: 1.82 TiB 
  ID-3: /dev/sdc vendor: HGST (Hitachi) model: HUS724020ALA640 
  size: 1.82 TiB 
Partition:
  ID-1: / size: 67.05 GiB used: 16.3 GiB (24.3%) fs: ext4 dev: /dev/md1 
  ID-2: /mnt/apps size: 288.19 GiB used: 391.2 MiB (0.1%) fs: ext4 
  dev: /dev/dm-3 
  ID-3: /mnt/backups size: 899.14 GiB used: 29.35 GiB (3.3%) fs: ext4 
  dev: /dev/dm-4 
  ID-4: /mnt/media size: 288.19 GiB used: 4.15 GiB (1.4%) fs: ext4 
  dev: /dev/dm-1 
  ID-5: /mnt/pad size: 95.94 GiB used: 1.35 GiB (1.4%) fs: ext4 
  dev: /dev/dm-2 
  ID-6: /var/mail size: 192.06 GiB used: 146.5 MiB (0.1%) fs: ext4 
  dev: /dev/dm-0
Swap:
  ID-1: swap-1 type: partition size: 1.95 GiB used: 0 KiB (0.0%) 
  dev: /dev/sdc2 
  ID-2: swap-2 type: partition size: 1.95 GiB used: 0 KiB (0.0%) 
  dev: /dev/sdb2 
  ID-3: swap-3 type: partition size: 1.95 GiB used: 0 KiB (0.0%) 
  dev: /dev/sda2 
  ID-4: swap-4 type: zram size: 256 MiB used: 0 KiB (0.0%) dev: /dev/zram0   
```

