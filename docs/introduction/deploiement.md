Le système est déployé avec [Yunohost](https://yunohost.org). 

Le service d'annuaire [LDAP](https://fr.wikipedia.org/wiki/LDAP_Data_Interchange_Format) pour le serveur Yunohost n'est pas compatibles avec toutes les applications hébergées. L'identification LDAP ne fonctionne que pour les mails (Roundcube), pour le Cloud Linux07 (Nextcloud) et pour Mypads Linux07 (Etherpad_Mypads).

> [!WARNING]
> Certains utilisateurs du Cloud Linux07 (Nextcloud) ont un compte gratuit de 1 Go, ils se sont inscrits par eux-même et leur compte à  été validé, ils ne sont pas connectés à l'annuaire LDAP.

Le serveur a été partitionner en LVM pour pouvoir alloué des volumes partitionnés à certains dossiers ou certaines applications, déplacés avec des `mount --bind`, sauf pour la partition `\var\mail` qui elle montée dans le fichier `/etc/fstab`.

**lsblk**

```
NAME              MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
sda                 8:0    0   1.8T  0 disk  
├─sda1              8:1    0  68.4G  0 part  
│ └─md1             9:1    0  68.3G  0 raid1 /
├─sda2              8:2    0     2G  0 part  [SWAP]
├─sda3              8:3    0   1.8T  0 part  
│ └─md3             9:3    0   1.8T  0 raid1 
│   ├─vg-mail     253:0    0 195.3G  0 lvm   /var/mail
│   ├─vg-data     253:1    0   293G  0 lvm   /mnt/media
│   ├─vg-CRYPTPAD 253:2    0  97.7G  0 lvm   /mnt/pad
│   ├─vg-APPS     253:3    0   293G  0 lvm   /mnt/apps
│   └─vg-backups  253:4    0 913.7G  0 lvm   /mnt/backups
├─sda4              8:4    0     1K  0 part  
└─sda5              8:5    0   1.9M  0 part  
sdb                 8:16   0   1.8T  0 disk  
├─sdb1              8:17   0  68.4G  0 part  
│ └─md1             9:1    0  68.3G  0 raid1 /
├─sdb2              8:18   0     2G  0 part  [SWAP]
└─sdb3              8:19   0   1.8T  0 part  
  └─md3             9:3    0   1.8T  0 raid1 
    ├─vg-mail     253:0    0 195.3G  0 lvm   /var/mail
    ├─vg-data     253:1    0   293G  0 lvm   /mnt/media
    ├─vg-CRYPTPAD 253:2    0  97.7G  0 lvm   /mnt/pad
    ├─vg-APPS     253:3    0   293G  0 lvm   /mnt/apps
    └─vg-backups  253:4    0 913.7G  0 lvm   /mnt/backups
sdc                 8:32   1   1.8T  0 disk  
├─sdc1              8:33   1  68.4G  0 part  
│ └─md1             9:1    0  68.3G  0 raid1 /
├─sdc2              8:34   1     2G  0 part  [SWAP]
└─sdc3              8:35   1   1.8T  0 part  
  └─md3             9:3    0   1.8T  0 raid1 
    ├─vg-mail     253:0    0 195.3G  0 lvm   /var/mail
    ├─vg-data     253:1    0   293G  0 lvm   /mnt/media
    ├─vg-CRYPTPAD 253:2    0  97.7G  0 lvm   /mnt/pad
    ├─vg-APPS     253:3    0   293G  0 lvm   /mnt/apps
    └─vg-backups  253:4    0 913.7G  0 lvm   /mnt/backups
zram0             252:0    0   256M  0 disk  [SWAP]
```

**/etc/fstab**

```
UUID=84123fb9-bde8-41dd-a9fc-afd4fe57b0e6	/	ext4	defaults	01
# partition apps mattermost mobilizon
UUID=cc55dbd0-1abc-411a-a7aa-8a4f6d5eaf50	/mnt/apps	ext4	defaults,nofail	  0	0
/mnt/apps/mobilizon         /home/yunohost.app/mobilizon     none       defaults,bind    0     0
/mnt/apps/mattermost        /home/yunohost.app/mattermost    none       defaults,bind    0     0
# partition backups borg__2
UUID=86131a6e-ed0d-4071-aa81-41b50e8d3a50	/mnt/backups	ext4	defaults,nofail	  0	0
# partition avec data nextcloud
UUID=4a5b5a94-88ea-4328-a05b-4a3fae339ed2	/mnt/media	ext4	defaults,nofail   0	  0
/mnt/media/nextcloud        /home/yunohost.app/nextcloud     none       defaults,bind    0      0
# partition mails
UUID=d0294479-075c-480b-972e-0131fef47a5d	/var/mail	ext4	defaults0	1
# partition apps cryptpad et etherpad_mypads
UUID=983ea66d-4732-431a-9d31-86eb85bb9431	/mnt/pad	ext4	defaults,nofail	 0	0
/mnt/pad/cryptpad           /var/www/cryptpad       none       defaults,bind       0      0
/mnt/pad/etherpad_mypads    /var/www/etherpad_mypads   none    defaults,bind       0      0
/mnt/pad/libreto            /var/www/libreto        none       defaults,bind       0      0  
# partition swap
UUID=7ed24519-ae94-459b-acff-af6b12a72c2a	swap	swap	defaults	00
UUID=37dc28a5-c4ab-4260-bd4f-d9006f40cfaa	swap	swap	defaults	00
UUID=f0ccd1dd-4592-43ac-85de-ee1c149dd688	swap	swap	defaults	00
```
