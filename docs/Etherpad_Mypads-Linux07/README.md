# Etherpad_MyPads

## Etherpad

Etherpad est un éditeur de texte libre en ligne fonctionnant en mode collaboratif et en temps réel. Il permet à plusieurs personnes de partager l'élaboration simultanée d'un texte, et d'en discuter en parallèle, via une messagerie instantanée. Il peut avoir des usages pédagogiques, notamment pour l'apprentissage collaboratif.

Le système ne requiert ni installation ni inscription, il suffit d'avoir une connexion Internet et un navigateur web, puisque c'est une application en ligne. Etherpad (la partie reçue par l'ordinateur client) est écrit en JavaScript.

N'importe quel utilisateur peut modifier un document, nommé pad. Chaque pad a sa propre URL, et chaque personne qui connaît ce lien peut modifier le contenu du pad et participer à la messagerie instantanée associée. Chaque participant est identifié par une couleur et un nom ou pseudo.

Le logiciel enregistre automatiquement le document, à très courts intervalles, ce qui permet à tous les participants d'avoir l'impression d'éditer instantanément entre eux. Ils peuvent aussi enregistrer des versions spécifiquement, des « images » (enregistrement à un moment donné de l'état actuel du document). Il est possible d'importer et de télécharger le document aux formats HTML, Open Document (OpenOffice ou LibreOffice), Microsoft Word, ou PDF.

Ainsi, les contributions de chacun apparaissent immédiatement sur l'écran de tous les participants, et chaque ajout qu'il fait est signalé par cette couleur.

Une fenêtre de messagerie instantanée est également disponible, elle aussi utilisant le système de couleur dans le nom des expéditeurs. Il est possible de la laisser sur le côté droit de l'écran, mais elle peut être cachée et invisible pour l'utilisateur s’il le souhaite. 

_source: Wikipédia_

## MyPads

MyPads est une extension qui permet de créer des groupes privés de collaboration avec Etherpad. 

>[!NOTE] 
> Avec Yunohost, seuls les utilisateurs Yunohost ont accès à leur espace dédié de collaboration. 
Cela ne les empêchent pas de créer dans leur espace un texte Public ouvert à la collaboration avec des personnes non-inscrites.
