# Les Onglets

## Favoris

La page de favoris vous permet de voir l’ensemble des éléments de MyPads que vous avez mis en favoris et éventuellement de les en supprimer.

## Mes listes utilisateurs

Vous trouverez ici toutes vos listes d’utilisateurs, rangées par date de création. Des informations résumées sont disponibles avec le nom de chaque liste et le nombre d’utilisateurs liés.

Avec les icônes d’actions, situées en haut à droite de chaque liste, vous pouvez modifier ou supprimer les listes. Davantage de détails sont disponibles sur le formulaire d’édition.

> [!WARNING]
> Les utilisateurs doivent avoir des comptes adhérents, on ne peut pas ajouter de personnes extérieures aux listes.

![Liste Utilisateurs](https://ahp.li/bed2b2e6d07a4fba2044.png)

On peut modifier une liste ou en créer une nouvelle en ajoutant des noms d'utilisateurs ou mail, ce doit être des nom d'utilisateurs Yunohost ou des mails liés à l'instance Yunohost ([at] linux07.fr pour les adhérents Linux07) qui ce sont déjà connectés à leur espace MyPads au moins une fois.

![Modifier liste utilisateurs](https://ahp.li/04c2d22e822c8dbc44cc.png)

> [!TIP]
> Les listes utilisateurs permettent ensuite d'être ajoutées à un dossier restreint ou un pad restreint avec les permissions choisies. 


## Mon profil

Tout changement dans votre profil nécessite votre mot de passe actuel. Merci de noter que :

* vous pouvez modifier toute information en utilisant le champ correspondant ; 
* laisser les champs de mot de passe et de confirmation vides n’aura pas d’effet sur votre mot de passe actuel ;

Ici vous pourrez ajouter une Organisation, choisir votre couleur par défaut pour le surlignage des textes collaboratifs, cacher les blocs d'aide si vous cochez la case correspondante.
![Profil](https://ahp.li/18acbb1af083e66dfdd3.png)

 ## Déconnexion

 Le bouton de déconnexion ne fonctionne pas toujours. Sinon, vous pouvez revenir au portail adhérent avec l'icône du logo Linux07 et vous déconnecter de l'instance Yunohost. En fermant le navigateur vous serez déconnectés.

