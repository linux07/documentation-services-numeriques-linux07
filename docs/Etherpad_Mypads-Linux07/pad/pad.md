# Pad

En cliquant sur un pad vous arriverez vers un éditeur collaboratif de texte

![Édition pad](https://ahp.li/c443272fd2a75c9df35b.png)

En haut se trouve une barre d'édition avec:
* caractère Gras,
* Italique, 
* Surligné, 
* Barré, 
* Liste ordonnée,
* Liste désordonnée,
* Indenter
* Désindenter
* Annuler la dernière édition
* Rétablir l'anulation
* Effacer le surlignage (attention cela ne peut être rétabli par la suite, le surlignage permet de voir qui a éditer des changements, à faire que quand la collaboration est finalisé), 
* Ajouter un tableau,
* Superscript pour faire de racine carré,
* Subscript, valeur en dessous,
* Le Style de la ligne éditée,
* Annoté la sélection (en sélectionnant une phrase ou un paragraphe on peut ajouter une note)

Devant se trouve un **+** pour voir plus de possibilités:
* Taille de la police
* Couleur des caractères
* Aligner à gauche
* Centrer
* Aligner à droite
* Justifier

==============================================================

1. Les icônes en haut à droite permettent de passer en plein écran ou d'ouvrir dans un nouvel onglet en plein écran aussi.
    ![Édition Plein écran](https://ahp.li/0687248772c0202d6155.png)

2. Le Style est ce qui permet d'avoir un menu comme ici à droite avec les Titres et sous-titres, on peut ensuite naviguer plus aisèment vers les paragraphes avec ce moyen.

3. En bas à droite des icônes qui permettent:
  * Afficher le nombre de mots
  * Importer/Exporter des formats différents
    
  ![Importer ou Exporter](https://ahp.li/14c3437a919e440d336e.png)
   
  * Historique Dynamique montre un historique des versions enregistrées auparavant et la possibilté de revenir en arrière
    
  ![Révision pad](https://ahp.li/9a033e6d9d0aa07220d2.png)
    
  * Enregistrer la révision
  * Paramètres du bloc-notes
    
  ![Paramètres du bloc-notes](https://ahp.li/bd9fcddabdcbbdfa17a2.png)
    
  * Partager ce bloc-notes qui donne 2 liens, un lien vers le pad (si le pad est restreint, seul les utilisateurs autorisés pourront y accéder en se connectant) et un lien iframe pour ajouter le pad dans un site sur une page web.
    
  ![lien pad](https://ahp.li/377323934e3c72187d3e.png)

4. Icône des utilisateurs pour ce pad, en cliquant dessus on peut aussi éditer son pseudo et changer sa couleur de surlignage

  ![utilisateurs pad](https://ahp.li/1fc8ec8dbc8cfffe3dea.png)

5. Clavardage, permet de discuter avec les utilisateurs connectés pendant un travail collaboratif, en cliquant dessus une fenêtre de discussion s'ouvre
    
  ![Clavardage](https://ahp.li/79738322c26c4963b79a.png)




