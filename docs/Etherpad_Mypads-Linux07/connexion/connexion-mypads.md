# Connexion à l'espace Mypads

L'espace Mypads est réservé aux utilisateurs Yunohost, donc aux adhérent·e·s

Dans un premier temps, il faut se connecter au [portail linux07](https://linux07.fr).

![Connexion portail Yunohost](https://ahp.li/ef8617e1cf3198a5ad20.png)

Une fois connecté vous aurez dans cet espace plusieurs tuiles avec les services disponibles de Yunohost.

Les tuiles ont été renommée avec un nom court correspondant à leur fonction pour plus de lisibilité. Pour Etherpad_Mypads, il s'agit de la tuile **Espace Textes Collaboratifs - Texte Collaboratif**

![Portail Linux07 Tuiles](https://ahp.li/6d1d69f027d8d9889828.png)

> [!NOTE]
> Le portail dans cet environnement Yunohost permet d'être connecté au SSO, vous êtes ainsi automatiquement authentifié pour les services **Cloud Linux07**(Nextcloud) et à votre boîte **Mails**(Roundcube). Pour l'espace **Mypads** vous devez néamoins vous réauthentifier avec le même nom d"utilisateur et même mot de passe.

En cliquant sur la tuile, vous arrivez sur une page avec ces possibilités

![Choix pad ou Mypads](https://ahp.li/8b089751145dc871a7db.png)

1. **Créer ou ouvrir un bloc-notes intitulé** permet de créer un texte collaboratif (public) avec le nom que vous choisirez ou d'en ouvrir un déjà créer avec le même nom.
2. **MyPads** permet quand à lui de vous connecter dans votre espace personnel de textes collaboratifs Mypads.

En cliquant sur MyPads vous devrez vous authentifié avec votre nom d'utilisateur Yunohost et votre mot de passe

![Connexion MyPads](https://ahp.li/0283c2d2590b22c00c92.png)

Une fois connecter vous arrivez sur l'onglet **Mes Dossiers**

![Mypads Mes Dossiers](https://ahp.li/73b7fdf2a15271be2bea.png)

1. Onglets Mes Dossiers & Pads, Mes Favoris, Mes listes Utilisateurs, Mon Profil, Déconnexion
2. Créer un nouveau dossier
3. Un champ de recherche par nom pour les Dossiers
4. Une colonne aves les Dossiers créés avec devant une icône en forme de clé pour modifier les paramètres du Dossier et une autre icône pour supprimer le dossier
5. Une colonne des Pads(textes) dans le dossier avec leur nombre une icône **+** pour en ajouter les permissions des pads (par défaut celles du dossier), le Nombre d'admistrateurs du dossier, un **+** pour en ajouter, le nombre d'utilisateurs et un **+** pour en ajouter.
6. Une colonne Labels, des étiquettes que l'on peut ajouter au dossier
7. Cette icône avec le logo de Linux07 sert à revenir au portail utilisateur Yunohost. Elle est présente aussi dans d'autres applications comme le Cloud ou les Mails, vous pouvez lz déplacer avec un clic droit dessus et en laissant appuyer en déplaçant la souris ou le pad.

Vous avez toujours une Aide qui s'affichera pour vous guider à droite (que l'on peut désactiver dans son profil).
