# Dossier Mypads

En cliquant sur un dossier, on va voir la liste des textes(pads) en-dessous.

![Dossier Mypads](https://ahp.li/c5386ee82968e8553586.png)

Les détails d’un dossier vous montrent :

* les options définies lorsque le dossier a été créé ou mis à jour ;
* la liste des pads créés pour ce dossier ;
* et la liste des administrateurs et utilisateurs de ce dossier.

À partir de là, vous pouvez :

* créer de nouveaux pads, les modifier, les supprimer ou les mettre en favoris ;
* partager l’administration de votre dossier avec d’autres utilisateurs ;
* inviter d’autres utilisateurs à voir et participer aux pads de ce dossier.

En dessous en descendant, vous avez la liste des Admistarteurs et Utilisateurs et la possibilité d'en ajouter ou d'en suprimer

![Administrateurs & Utilisateurs](https://ahp.li/e5a35af69fbffadc3ff5.png)