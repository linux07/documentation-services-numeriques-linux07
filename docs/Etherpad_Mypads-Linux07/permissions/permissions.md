# Configurations des Dossiers et Pads

## Modifier un Dossier

![Modifier un Dossier](https://ahp.li/abaea5fed9144a8ed65e.png)

### Accès

Vous avez le choix entre trois niveaux de visibilité. Cela impactera tous les pads liés :

* restreint, le choix par défaut : l’accès aux pads sera limité à une liste d’utilisateurs invités que vous aurez choisis ;
* privé : dans ce mode vous aurez à saisir un mot de passe qui sera vérifié pour autoriser l’accès ;
* public : dans ce mode, tous les pads sont publics, les utilisateurs n’ont alors besoin que de l’adresse URL pour y accéder.

### Lecture seule

Si vous activez la lecture seule, tous les pads attachés conserveront leur dernier état connu et ne pourront être édités. Notez que la visibilité fonctionnera toujours.

### Labels

Vous pouvez associer des labels à cet élément en cliquant sur le champ correspondant et en les sélectionnant un par un.

Pour créer un nouveau label, saisissez-le et appuyez sur la touche ENTRÉE ou cliquez sur le bouton « Ok ». Une fois le label ajouté, il sera automatiquement sélectionné sur ce formulaire.

Vous pouvez supprimer un label en cliquant sur la croix située à sa droite.

## Modifier un Pad

![Modifier un pad](https://ahp.li/63076c662e920198972c.png)

Par défaut les pads créés ont les mêmes paramètres que le dossier, pour les changer on peut déchocher **Paramètres du Dossier**

![Modifier paramètres pad par défaut](https://ahp.li/a16b06f5925a6e512a11.png)

Dans ce cas comme pour un dossier: 

### Accès

Vous avez le choix entre trois niveaux de visibilité. Cela impactera tous les pads liés :

* restreint, le choix par défaut : l’accès aux pads sera limité à une liste d’utilisateurs invités que vous aurez choisis ;
* privé : dans ce mode vous aurez à saisir un mot de passe qui sera vérifié pour autoriser l’accès ;
* public : dans ce mode, tous les pads sont publics, les utilisateurs n’ont alors besoin que de l’adresse URL pour y accéder.

### Lecture seule

Si vous activez la lecture seule, tous les pads attachés conserveront leur dernier état connu et ne pourront être édités. Notez que la visibilité fonctionnera toujours.

### Labels

Vous pouvez associer des labels à cet élément en cliquant sur le champ correspondant et en les sélectionnant un par un.

Pour créer un nouveau label, saisissez-le et appuyez sur la touche ENTRÉE ou cliquez sur le bouton « Ok ». Une fois le label ajouté, il sera automatiquement sélectionné sur ce formulaire.

Vous pouvez supprimer un label en cliquant sur la croix située à sa droite.